\chapter{L'analisi di complessità degli algoritmi}
L'\textbf{analisi di complessità} è un metodo formale per prevedere le risorse richieste dall'algoritmo (cioè il tempo di esecuzione e la quantità di memoria utilizzata).

\begin{itemize}
\item Il tempo però non è un tempo fisico o misurato sperimentalmente su un processore, ma dipende dal numero di passi $\Rightarrow$ l'analisi di complessità è \textbf{indipendente dalla macchina}. Si assume che la macchina lavori in modo sequenziale e non parallelo (architettura di Von Neumann). Un algoritmo meno complesso su una macchina più lenta può essere più veloce.
\item L'analisi è \textbf{indipendente da quali dati in ingresso} ci sono ma è \textbf{dipendente} solo \textbf{da quanti dati in ingresso} ci sono, cioè più in generale dalla \textbf{dimensione $n$} del problema.
\end{itemize}

\paragraph{Output}
\begin{itemize}
\item $S \left(n\right)$: memoria richiesta
\item $T \left(n\right)$: tempo di esecuzione
\end{itemize}

\paragraph{Classificazione degli algoritmi per complessità} (dal meno complesso al più complesso)
\begin{itemize}
\item costante: $1$
\item logaritmico: $\log{n}$
\item lineare: $n$
\item linearitmico: $n \log{n}$
\item quadratico: $n^2$
\item cubico: $n^3$
\item esponenziale: $2^n$
\end{itemize}

\section{Analisi di complessità di caso peggiore}
L'analisi di complessità è detta:
\begin{itemize}
\item \textbf{asintotica} se il numero di dati tende a infinito;
\item \textbf{di caso peggiore} se il tempo stimato non può che essere maggiore o uguale al tempo effettivo su uno specifico dato.
\end{itemize}

Si stima il caso peggiore perché è quello più frequente generalmente, e perché il caso medio spesso è difficile da calcolare o coincide con il peggiore.

\paragraph{Ricerca sequenziale}
\[
T\left(n\right) \propto n
\]

\paragraph{Ricerca dicotomica}
Bisogna considerare il caso peggiore: la chiave non si trova. Alla $i$-esima iterazione, la lunghezza del sottovettore è uguale a $\frac{1}{2^i}$. La terminazione avviene quando il vettore è lungo $1 = \frac{1}{2^i};\;i = \log_2{n}$.

\paragraph{Insertion sort}
Nel caso peggiore, ad ogni iterazione del ciclo esterno si fanno $i - 1$ scansioni del vettore, e all'ultima se ne fanno $n - 1$:
\[
T\left(n\right) = 1 + 2 + \ldots + \left( n - 2 \right) + \left( n - 1 \right) = \frac{n \left( n - 1 \right)}{2} \rightarrow T\left(n\right) \propto n^2
\]

\section{Notazioni}
\subsection[O grande]{O grande ($O$)}
\[
T\left(n\right) = O\left(g\left(n\right)\right) \Leftrightarrow \exists c > 0, \, n_0 > 0 : \forall n \geq n_0 \rightarrow 0 \leq T\left(n\right) \leq c \cdot g\left(n\right)
\]

$g\left(n\right)$ è un \textbf{limite superiore lasco}:
\begin{itemize}
\item superiore: è una funzione maggiorante, a partire da un certo $n_0$;
\item lasco: non scresce come $g\left(n\right)$, ma al più come $g\left(n\right)$.
\end{itemize}

Se $T\left(n\right)$ è un polinomio in $n$ di grado $m$ $\Rightarrow$ $T\left(n\right) = O\left(n^m\right)$.

\subsection[Omega grande]{Omega grande ($\Omega$)}
\[
T\left(n\right) = \Omega \left(g \left( n \right) \right) \Leftrightarrow \exists c > 0, \, n_0 > 0 : \forall n \geq n_0 \rightarrow 0 \leq c \cdot g\left(n\right) \leq T\left(n\right)
\]

$g\left(n\right)$ è il \textbf{limite inferiore lasco} della complessità asintotica di caso peggiore ($\neq$ caso migliore). Se si dimostra che, per una certa operazione, la $\Omega$ è maggiore della complessità lineare, non si potrà mai trovare un algoritmo lineare che svolga quell'operazione.

$T\left(n\right)$ polinomio $\Rightarrow$ $T\left(n\right) = \Omega \left( n^m \right)$

\subsection[Theta grande]{Theta grande ($\Theta$)}
\[
T\left(n\right) = \Theta \left(g \left( n \right) \right) \Leftrightarrow \exists c_1, \, c_2 \, n_0 > 0 : \forall n \geq n_0 \rightarrow 0 \leq c_1 \cdot g\left(n\right) \leq T\left(n\right) \leq c_2 \cdot g\left(n\right)
\]

$g\left(n\right)$ è il \textbf{limite asintotico stretto} di $T\left(n\right)$: $T\left(n\right)$ cresce come $g\left(n\right)$.

È la combinazione dei due precedenti:
\begin{itemize}
\item $T\left(n\right)$ polinomio $\Rightarrow$ $T\left(n\right) = \Theta\left(n^m\right)$;
\item $T\left(n\right) = \Theta\left(g\left(n\right)\right) \leftrightarrow T\left(n\right) = O\left(g\left(n\right)\right) \wedge T\left(n\right) = \Omega\left(g\left(n\right)\right)$
\end{itemize}

\section{Online connectivity}
L'\textbf{online connectivity} è un problema avente per input un insieme di coppie di interi $p$ e $q$ (per es. i nodi di una rete). ($p$, $q$) definisce una relazione di connessione tra i nodi $p$ e $q$.

\paragraph{Proprietà}
\begin{itemize}
\item \textbf{commutativa:} se $p$ è connesso con $q$ $\Rightarrow$ $q$ è connesso con $p$;
\item \textbf{transitiva:} se $p$ è connesso con $q$ $\wedge$ $q$ è connesso con $r$ $\Rightarrow$ $p$ è connesso con $r$.
\end{itemize}

Le coppie possono essere:
\begin{itemize}
\item mantenute: la connessione della coppia non è ancora nota (output: coppia ($p$, $q$));
\item scartate: la coppia è già connessa, anche per la proprietà transitiva o commutativa (output: nullo).
\end{itemize}

\paragraph{Applicazioni}
\begin{itemize}
\item reti di computer: ottimizzazione togliendo le connessioni ridondanti;
\item reti elettriche;
\item linguaggi di programmazione: variabili equivalenti.
\end{itemize}

La struttura dati a \textbf{grafo} è costituita da 2 insiemi: nodi $p$ e $q$ e archi.

Per ottimizzare il grafo, si verifica di volta in volta se la connessione della coppia in input non è ancora nota o se è implicata già dalle precedenti. L'analisi di questo genere presuppone l'esistenza del grafo $\Rightarrow$ non è un'analisi di connettività online.

La connettività si dice \textbf{online} se si sta lavorando non su una struttura o su un modello pre-esistente, ma su un grafo che viene costruito arco per arco.

\begin{enumerate}
\item \ul{ipotesi:} si conoscono i vertici, e ogni vertice è connesso solo a se stesso $\Rightarrow$ nessun arco;
\item \textbf{operazione find} (ricerca): a ogni coppia in input, verifica se $p$ e $q$ appartengono allo stesso insieme (2 find);
\item \textbf{operazione union} (unione): se $p$ e $q$ non erano connessi, unisci l'insieme a cui appartiene $p$ ($S_p$) e l'insieme a cui appartiene $q$ ($S_q$).
\end{enumerate}

L'online connectivity può essere implementata tramite vettori:
\begin{itemize}
\item \textbf{quick-find:} find rapide, union lente;
\item \textbf{quick-union:} find lente, union rapide.
\end{itemize}

\subsection{Quick-find}
\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{pic/02/Quick-find.png}
\end{figure}

\noindent
A seconda del contenuto della cella del vettore:
\begin{itemize}
\item se è il suo stesso indice $i$: il vertice $i$-esimo è connesso a se stesso;
\item se è un altro indice: i due vertici appartengono allo stesso insieme.
\end{itemize}

\paragraph{Complessità}
\begin{itemize}
\item \ul{find:} accesso a una cella di vettore tramite il suo indice: $O\left(1\right)$
\item \ul{union:} scansione del vettore per cambiare da $p$ a $q$: $O\left(n\right)$
\end{itemize}

Complessità di caso peggiore totale: numero di coppie $\times$ dimensioni vettore ($n$)

riferimenti diretti (nessuna catena) $\Rightarrow$ si trova subito il rappresentante
\FloatBarrier

\subsection{Quick-union}
\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/02/Quick-union.png}
\end{figure}

\noindent
C'è una catena di riferimenti di tipo indiretto. A seconda del contenuto della cella del vettore:
\begin{itemize}
\item se è uguale al suo indice $i$: fine catena;
\item se è diverso dal suo indice: passa alla cella $i$-esima a cui punta: \texttt{id[id[...id[\textsl{i}]]...] = (id[\textsl{i}])*}
\end{itemize}

\paragraph{Complessità}
\begin{itemize}
\item \ul{find:} due elementi sono connessi se \texttt{(id[\textsl{i}])* = (id[\textsl{j}])*}: $T\left(n\right) = O\left(n\right)$
\item \ul{union:} la testa della prima catena punta alla testa della seconda, cioè \texttt{(id[\textsl{p}])* = (id[\textsl{q}])*} $\Rightarrow$ l'albero si frastaglia: $T\left(n\right) = O\left(1\right)$
\end{itemize}

Nella find si deve perciò percorrere tutta la catena fino alla testa, la union è immediata (unione solo delle teste).
%\FloatBarrier
