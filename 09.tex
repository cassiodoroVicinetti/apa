\chapter{Alberi binari di ricerca (BST)}
\section{Operazioni su alberi binari}
\subsection{Attraversamento di un albero binario}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.25\linewidth]{pic/09/Attraversamento_albero_binario.png}
\end{figure}

\paragraph{Strategie di visita degli alberi binari} (Root = radice, L = sottoalbero sinistro, R = sottoalbero destro)
\begin{itemize}
\item \textbf{pre order:} Root, L, R
\item \textbf{in order:} L, Root, R
\item \textbf{post order:} L, R, Root
\end{itemize}

L'operazione di attraversamento ha complessità lineare.

\subsection{Calcolo ricorsivo di parametri}
\begin{itemize}
\item \ul{numero di nodi:} 1 Root + ricorsione(L) + ricorsione(R)
\item \ul{altezza:} 1 + altezza del sottoalbero maggiore (calcolata in modo ricorsivo)
\end{itemize}

\section{Alberi binari di ricerca (BST)}
\begin{description}
\item[albero binario di ricerca] albero binario in cui, per ogni radice, si trovano nodi le chi chiavi sono minori o uguali nel sottoalbero sinistro e nodi le cui chiavi sono maggiori o uguali in quello destro $\Rightarrow$ la radice è l'elemento di separazione tra dati (chiavi) minori a sinistra e maggiori a destra.
\end{description}

\subsection{Operazioni di base}
\subsubsection{Search}
Ricerca una chiave, determinando a ogni radice il sottoalbero in cui cercare con un semplice confronto.

\paragraph{Casi di terminazione}
\begin{itemize}
\item \ul{search hit:} la chiave è stata trovata in una radice;
\item \ul{search miss:} la chiave non è stata trovata e si è giunti a un albero vuoto.
\end{itemize}

\subsubsection{Min/Max}
Ricerca il valore minimo/massimo, percorrendo tutti i sottoalberi sinistri/destri.

\subsubsection{Sort}
Per ottenere l'ordinamento crescente delle chiavi basta visitare il BST in order. Per fare questo è necessario visitare tutti i nodi dell'albero: l'operazione ha quindi complessità lineare nel numero di nodi.

\subsubsection{Successor}
\begin{enumerate}
\item[1\textsuperscript{$\circ$} modo)] Si ordinano i valori nell'albero con l'operazione Sort.
\item[2\textsuperscript{$\circ$} modo)] Il successore è l'elemento che tra le chiavi più grandi ha la chiave più piccola $\Rightarrow$ è il minimo del sottoalbero destro. Se il sottoalbero destro è vuoto, si cerca il primo antenato che abbia come figlio sinistro il nodo stesso o un suo antenato.
\end{enumerate}

\subsubsection{Predecessor}
\begin{enumerate}
\item[1\textsuperscript{$\circ$} modo)] Si ordinano i valori nell'albero con l'operazione Sort.
\item[2\textsuperscript{$\circ$} modo)] Il predecessore è l'elemento che tra le chiavi più piccole ha la chiave più grande $\Rightarrow$ è il massimo del sottoalbero sinistro. Se il sottoalbero sinistro è vuoto, si cerca il primo antenato che abbia come figlio destro il nodo stesso o un suo antenato.
\end{enumerate}

\subsubsection{Insert}
Inserisce un nuovo elemento mantenendo le proprietà del BST. L'inserzione avviene sempre nelle foglie.

Se il BST è vuoto, crea un nuovo albero, altrimenti:
\begin{itemize}
\item \ul{inserzione ricorsiva:} considera ricorsivamente terne L-Root-R, e a ogni passo effettua un confronto;
\item \ul{inserzione iterativa:} prima ricerca (search) la posizione in cui la chiave si dovrebbe trovare, quindi la inserisce in quella posizione.
\end{itemize}

\subsubsection{Select}
Dato un valore intero $k$, estrae/sceglie la $k + 1$-esima chiave più piccola nel BST (con $k$ che parte da 0). Dopo aver associato a ogni nodo il numero delle chiavi contenute nei sottoalberi radicati in esso,\footnote{Si calcola in questo modo:
\begin{itemize}
\item ogni foglia è costituita da 1 chiave;
\item procedendo dal basso verso l'alto (bottom-up), si somma a ogni passo le dimensioni dei sottoalberi e si aggiunge 1 per la radice.
\end{itemize}} a ogni terna L-Root-R determina se la chiave che si sta cercando può essere contenuta nel sottoalbero L in base al suo numero di chiavi associato t (per il sottoalbero sinistro vuoto: $t = 0$):
\begin{itemize}
\item se $t = k$: termina l'operazione di select e ritorna Root;
\item se $t > k$: scende nel sottoalbero L;
\item se $t < k$: scende nel sottoalbero R, ricercando la chiave di ordine $k = k - t - 1$.
\end{itemize}

\subsubsection{Complessità}
Tutte queste operazioni, tranne la visita (sopra denominata ``sort''), sui BST sono di complessità lineare\footnote{Ad esempio, nel caso peggiore i confronti dell'operazione di search vengono fatti fino in fondo all'albero.} rispetto all'altezza dell'albero: $T \left( n \right) = O \left( h \right)$ $\Rightarrow$ rispetto a $n$. Il BST è vantaggioso tanto più l'albero è bilanciato:
\begin{itemize}
\item caso migliore: $h= \log_2{n} \Rightarrow T \left( n \right) =O \left( \log{n} \right)$ (albero completamente bilanciato)
\item caso medio: $T \left( n \right) \simeq O \left( \log{n} \right)$
\item caso peggiore: $h=n \Rightarrow T \left( n \right) =O \left( n \right)$ (albero completamente sbilanciato)
\end{itemize}

Effettuando tante operazioni su un BST bilanciato, il BST si potrebbe però sbilanciare:

\begin{itemize}
\item[1\textsuperscript{a} soluzione)] ogni tanto si ribilancia il BST $\Rightarrow$ poco efficace, perché si aggiunge la complessità dell'operazione di ribilanciamento;
\item[2\textsuperscript{a} soluzione)] si costruisce un albero bilanciato per costruzione, applicando dei vincoli.
\end{itemize}

\subsection{Operazioni di servizio}
\subsubsection{Rotate a destra/sinistra}
Si scambia il rapporto padre-figlio tra due nodi $x$ e $y$, posizionando opportunamente i sottoalberi di partenza dei nodi attraverso semplici spostamenti dei puntatori ai sottoalberi.

\subsection{Operazioni avanzate}
\subsubsection{Inserimento alla radice}
Inserisce la chiave con l'operazione Insert, quindi effettua delle rotazioni per spostare progressivamente la nuova chiave dal basso verso la radice.

\subsubsection{Partition}
Riorganizza l'albero intorno a una certa chiave di ordine $k$ (Select), portandola alla radice (Rotate). Se applicata intorno alla chiave mediana, spesso permette di ribilanciare un BST.

\subsubsection{Delete}
\begin{itemize}
\item nodo senza figli (foglia): si può cancellare subito la foglia;
\item nodo con 1 figlio: basta connettere il figlio del nodo con il padre del nodo;
\item nodo con 2 figli: bisogna sostituirlo o con il suo predecessore o con il suo successore:
\begin{itemize}
\item \ul{Precedessor/Successor:} si ricerca il predecessore/successore in uno dei sottoalberi;
\item \ul{Partition:} lo si fa galleggiare fino alla radice;
\item lo si connette con l'altro sottoalbero.
\end{itemize}
\end{itemize}
