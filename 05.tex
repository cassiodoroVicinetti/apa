\chapter{La ricorsione}
\begin{itemize}
\item \textbf{ricorsione diretta:} nella definizione di una procedura si richiama la procedura stessa
\item \textbf{ricorsione indiretta:} nella definizione di una procedura si richiama una o più procedure che richiamano la procedura stessa
\end{itemize}

Si rompe ricorsivamente un problema in analoghi sottoproblemi più semplici, fino a quando non si arriva alla soluzione di un problema elementare (si veda il capitolo~\ref{cap:divide_et_impera}).

Non è una definizione circolare (all'infinito), ma c'è la condizione di terminazione (ricorsione finita).

\section{Esempi}
\subsection{Fattoriale}
La definizione stessa di fattoriale è di tipo ricorsivo.

A ogni passo si genera un unico sottoproblema anziché due $\Rightarrow$ non sarà un albero binario completo.

\subsection{Numeri di Fibonacci}
\begin{figure}
	\centering
	\includegraphics[width=0.55\linewidth]{pic/05/Fibonacci.png}
\end{figure}

\noindent
Un numero di Fibonacci ${\text{FIB}}_{n + 1}$ è la somma dei due che lo precedono nell'ordinamento: ${\text{FIB}}_{n - 1}$ e ${\text{FIB}}_n$.

La ratio aurea\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Sezione_aurea}{Sezione aurea} su Wikipedia in italiano.} è il rapporto tra un segmento maggiore $a$ e uno minore $b$: $\varphi = \frac{a}{b}$. Il segmento maggiore $a$ è il medio proporzionale tra il minore $b$ e la somma dei due:
\[
\left( a + b \right) : a = a : b ; \; \frac{ \varphi + 1 } {\varphi} = \varphi; \; {\varphi}^2 - \varphi - 1 = 0; \; \varphi = \frac{1 + \sqrt{5}}{2} \vee \varphi ' = \frac{1 - \sqrt{5}}{2}
\]

Ogni numero di Fibonacci ha la seguente relazione con la ratio aurea $\varphi$:
\[
\text{FIB}_n = \frac{{\varphi}^n - {\varphi'}^n}{\sqrt{5}}
\]

La ricorsione non procede in modo parallelo per livelli dell'albero, ma è un'analisi in profondità dell'\textbf{albero della ricorsione}, ovvero si segue ogni cammino fino alla foglia = soluzione elementare.
\FloatBarrier

\subsection{Algoritmo di Euclide}
Dati $m$ e $n$, permette di calcolare il Massimo Comun Divisore:
\begin{itemize}
\item se $m > n$ $\Rightarrow$ $\text{MCD} \left( m, n \right) = \text{MCD} \left( n, m \text{ mod } n \right)$
\item condizione di terminazione: se $n = 0$ ritorna $m$
\end{itemize}

\subsection[Valutazione di espressione in forma prefissa]{Valutazione di espressione in forma prefissa\footnote{Per approfondire, si veda la voce \href{https://it.wikipedia.org/wiki/Notazione_polacca}{Notazione polacca} su Wikipedia in italiano.}}
Ponendo per semplicità di trattare solo con operandi di somma e prodotto di arità\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Arietà}{Arietà} su Wikipedia in italiano.} 2, una espressione può essere definita ricorsivamente in funzione di se stessa: \includegraphics[scale=0.22]{pic/05/Forma_prefissa.png}, con condizione di terminazione: l'espressione è uno degli operandi.

Inserendo l'espressione in un vettore \includegraphics[scale=0.22]{pic/05/Vettore_forma_prefissa.png}, a ogni passo si valuta \texttt{a[i]}:
\begin{itemize}
\item se \texttt{a[i]} è un operatore, valuta l'espressione a destra dell'operatore;
\item condizione di terminazione: se \texttt{a[i]} è un numero, ritornalo.
\end{itemize}

\subsection{Ricerca binaria}
Si considera a ogni passo della ricorsione un sottovettore di metà dimensione, anch'esso ordinato.

\subsection{Ricerca del massimo di un vettore}
A ogni passo, si suddivide il vettore in due sottovettori, si recupera il massimo di ciascun sottovettore, e si confrontano i risultati. Si termina quando il sottovettore ha un solo elemento.

\subsection{Moltiplicazione rapida di 2 interi}
\begin{itemize}
\item[1\textsuperscript{$\circ$} modo)] Seguo la definizione ricorsiva: $\begin{cases} x \cdot y = x + x \cdot \left( y - 1 \right) \\
x \cdot 1 = x \end{cases}$
\item[2\textsuperscript{$\circ$} modo)] Si assume:
\begin{itemize}
\item si sa calcolare il prodotto solo di cifre decimali (come se si avessero solo le tavole pitagoriche\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Tavola_pitagorica}{Tavola pitagorica} su Wikipedia in italiano.});
\item per semplicità che $x$ e $y$ abbiano $x = 2^k$ cifre.
\end{itemize}

Si divide $x$ in due sottovettori $x_s$ e $x_d$ di metà dimensione, e così pure $y$ in $y_s$ e $y_d$ $\Rightarrow$ $\begin{cases} x = x_s \cdot {10}^{\frac{n}{2}} + x_d \\
y = y_s \cdot {10}^{\frac{n}{2}} + y_d \end{cases}$

\[
x \cdot y = \left( x_s \cdot {10}^{\frac{n}{2}} + x_d \right) \cdot \left( y_s \cdot {10}^{\frac{n}{2}} + y_d \right) = x_s \cdot y_s \cdot {10}^n + x_s \cdot y_d \cdot {10}^{\frac{n}{2}} + x_d \cdot y_s \cdot {10}^{\frac{n}{2}} + x_d \cdot y_d
\]

Si continua a suddividere ciascun sottovettore fino alla condizione di terminazione: i fattori hanno una sola cifra. 

A ogni passo, si generano 4 sottoproblemi di dimensione metà $\Rightarrow$ si richiama per 4 volte l'operazione di moltiplicazione.
\end{itemize}

\subsection{Moltiplicazione rapida di 2 interi ottimizzata}
Il numero di moltiplicazioni richiamate a ogni passo si riduce a 3:
\[
x_s \cdot y_d+x_d \cdot y_s=x_s \cdot y_s+x_d \cdot y_d- \left( x_s-x_d \right) \cdot \left( y_s-y_d \right)
\]

\section{Liste}
\subsection{Definizioni}
\begin{description}
\item[sequenza lineare] insieme finito di elementi, ciascuno associato a un indice univoco, in cui conta la posizione reciproca tra di essi (cioè ogni elemento ha un successore e un predecessore)
\end{description}

\begin{enumerate}
\item \textbf{accesso diretto:} (implementazione tramite vettore) locazioni di memoria contigue accessibili tramite indice $\Rightarrow$ complessità $O \left( 1 \right)$;
\item \textbf{accesso sequenziale:} l'accesso a un certo elemento necessita della scansione sequenziale della lista a partire dal primo elemento $\Rightarrow$ complessità $O \left( n \right)$.
\end{enumerate}

Una lista è una sequenza lineare ad accesso sequenziale. La lista è un concetto astratto, e si può implementare in C:
\begin{itemize}
\item tramite un vettore: lo posso usare anche per dati in locazioni di memoria non contigue;
\item tramite un sistema a puntatori \includegraphics[scale=0.22]{pic/05/Lista_puntatori.png}: in questo caso la lista si dice concatenata.
\end{itemize}

\begin{description}
\item[lista concatenata] insieme di elementi dello stesso tipo (dati), memorizzati in modo non contiguo (nodi, implementati tramite struct), accessibili mediante riferimento (link $\Rightarrow$ puntatori) al successore/precedente. La memoria viene allocata e liberata dinamicamente per ogni nodo ($\Rightarrow$ i dati possono teoricamente essere infiniti), ma l'accesso non è diretto.
\end{description}

\subsection{Classificazione}
\begin{itemize}
\item ordinata / non ordinata
\item circolare / con testa e coda
\item singolo-linkata / doppio-linkata (senza/con link a successore)
\end{itemize}

\subsection{Ricorsione}
\paragraph{Definizione ricorsiva} Una lista è un elemento seguito da una lista. Funzioni:
\begin{itemize}
\item \ul{conteggio:} a ogni passo: 1 + numero degli elementi della sottolista considerata a partire dal successore;
\item \ul{attraversamento:} elenca gli elementi in una lista con una strategia (ordine) predefinita di percorrimento;
\item \ul{eliminazione} di un elemento.
\end{itemize}

Non sempre la soluzione ricorsiva è più efficiente di quella iterativa.