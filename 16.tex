\chapter{I cammini minimi}
Si considerano grafi orientati e pesati. Il \textbf{peso $w$ di un cammino} è la sommatoria dei pesi associati agli archi che compongono il cammino. Il \textbf{peso minimo $\sigma$ di un cammino} tra $u$ e $v$ è uguale al minimo peso di tutti i cammini tra $u$ e $v$ se almeno uno esiste, altrimenti il peso è $+ \infty$. Più cammini possono essere tutti di peso minimo, ma il peso minimo è univoco.

\section{Applicazioni}
Si applica per esempio a reti di città con pesi corrispondenti alle distanze.

\paragraph{Casi}
\begin{itemize}
\item da sorgente singola: a partire da una città si calcolano i cammini minimi verso tutte le altre destinazioni;
\item con destinazione singola: viceversa $\Rightarrow$ basta risolvere con lo stesso algoritmo lavorando sul grafo trasposto;
\item tra tutte le coppie di vertici: si calcolano i cammini minimi tra tutte le coppie di vertici (matrice triangolare) $\Rightarrow$ basta iterare per tutti i vertici l'algoritmo con una sorgente singola.
\end{itemize}

\section{Grafi con archi a peso negativo}
Si distinguono due casi:
\begin{enumerate}
\item nessun arco (di peso negativo) appartiene a un ciclo con somma dei pesi negativa:
\begin{itemize}
\item algoritmo di Dijkstra: non garantisce la soluzione ottima (algoritmo greedy);
\item algoritmo di Bellman-Ford: garantisce comunque la soluzione ottima;
\end{itemize}
\item esiste almeno un arco (di peso negativo) appartenente a un ciclo con somma dei pesi negativa:\footnote{In realtà non ha neanche senso parlare di ricerca di cammini minimi perché percorrendo il ciclo si arriva a una stima $- \infty$.}
\begin{itemize}
\item algoritmo di Dijkstra: il risultato non ha senso perché non ha senso il problema, e non rileva neanche il ciclo a peso negativo;
\item algoritmo di Bellman-Ford: è in grado di rilevare il ciclo a peso negativo.
\end{itemize}
\end{enumerate}

\section{Rappresentazione}
\begin{itemize}
\item \ul{vettore dei predecessori:} per il nodo $i$-esimo riporta l'indice del padre se esiste, altrimenti $-1$;
\item \ul{sottografo dei predecessori $G_\pi \left( V_\pi , E_\pi \right)$:} $V_\pi$ contiene tutti i vertici per cui esiste il padre + il vertice di partenza, $E_\pi$ contiene gli archi che connettono tutte le coppie di vertici padre-figlio di $V_\pi$ tranne il vertice di partenza;
\item \ul{albero dei cammini minimi $G' = \left( V' , E' \right)$:} $V'$ contiene tutti i vertici raggiungibili dalla radice $s$ dell'albero $\Rightarrow$ siccome è un albero, esiste un solo cammino semplice che connette ogni vertice con la radice:
\begin{itemize}
\item se il grafo non è pesato (o di pesi unitari), basta fare una visita in ampiezza;
\item se il grafo è pesato, durante la visita del grafo si utilizza una coda a priorità per rappresentare i pesi.
\end{itemize}
\end{itemize}

\section{Relaxation}
\subsection{Fondamenti teorici}
Posti due vertici $v_i$ e $v_j$ connessi da uno o più cammini per cui esiste un cammino minimo, si può prendere un qualsiasi sottocammino di quel cammino minimo. Se quel sottocammino non fosse ottimo, esisterebbe un altro sottocammino ottimo che abbasserebbe la stima del cammino complessivo $\Rightarrow$ non è possibile $\Rightarrow$ ogni sottocammino di un cammino minimo è minimo.

Un cammino ottimo da $v_i$ a $v_j$ è esprimibile attraverso il sottocammino minimo da $v_i$ a $v_{j-1}$ + l'arco da $v_{j-1}$ a $v_j$ (non si trattano multigrafi $\Rightarrow$ non si hanno più archi tra due stessi vertici).

\subsection{Procedimento}
Usando un vettore dei pesi \texttt{wt}, inizialmente la radice $s$ dista 0 da se stessa, e tutti i vertici distano $+ \infty$. A ogni passo si effettua la \textbf{relaxation}: si confronta con la stima precedente il peso del sottocammino minimo fino a $j-1$ + il peso dell'arco $\Rightarrow$ se la stima è migliore si prende la nuova distanza minima stimata, fino a quando non si raggiunge la distanza minima definitiva, poiché una relaxation effettuata su un cammino già minimo non ha più effetto.

L'algoritmo di Dijkstra applica una sola relaxation per ogni arco, e usa la coda a priorità; l'algoritmo di Bellman-Ford applica tante relaxation per arco quanti sono i vertici, e prima deve ordinare tutti i vertici.

\section{Algoritmo di Dijkstra}
A ogni passo si considerano in maniera greedy i vertici non ancora stimati (= cioè appartenenti a $V-S$) raggiungibili dal vertice corrente, su cui poter applicare la relaxation. Usa una coda a priorità, dove la priorità corrisponde alla distanza minima correntemente stimata, in cui ogni vertice viene estratto una sola volta e non ristimando mai più di una volta una stima. Condizione di terminazione: la coda a priorità è vuota.

\paragraph{Complessità}
\[
T \left( n \right) = O \left( \left( \left| V \right| + \left| E \right| \right) \log{\left| V \right|} \right)
\]

Se esiste un arco negativo, esso consentirebbe di ristimare la stima su un vertice già precedentemente stimato, ma quel vertice non entrerà mai più nella coda a priorità.

\section{Algoritmo di Bellman-Ford}
\begin{enumerate}
\item si ordinano gli archi attraverso un criterio di ordinamento stabilito;
\item per ogni vertice si applicano $\left| V \right| -1$ relaxation;
\item alla fine si verifica che per tutte le $\left| V \right|$-esime relaxation non migliorino alcuna stima, perché altrimenti esiste almeno un ciclo negativo.
\end{enumerate}