\chapter{Il paradigma ``divide et impera''}
\label{cap:divide_et_impera}
\begin{enumerate}
\item \textbf{divide:} un problema di dimensione $n$ viene ripartito in a sottoproblemi, ciascuno di dimensione $\frac{n}{b}$, tra loro indipendenti ($\Leftrightarrow$ ricorsione su ogni sottoproblema);
\item \textbf{impera:} quando si arriva alla soluzione elementare, avviene la risoluzione diretta ($\Leftrightarrow$ condizione di terminazione);
\item \textbf{combina:} le soluzioni parziali (elementari) vengono ricombinate per ottenere la soluzione del problema di partenza.
\end{enumerate}

\section{Equazioni alle ricorrenze}
L'\textbf{equazione alle ricorrenze} permette di analizzare la complessità di ogni passo del procedimento divide et impera:
\[
\begin{cases} T \left( n \right) = D \left( n \right) + a \cdot T \left( \frac{n}{b} \right) + C \left( n \right) , \quad n > c \\
T \left( n \right) = \Theta \left( 1 \right) , \quad n \leq c \end{cases}
\]

\begin{enumerate}
\item \ul{divide:} $D \left( n \right)$ (costo della divisione)
\item \ul{impera:} $\Theta \left( 1 \right)$ (costo della soluzione elementare)
\item \ul{combina:} $C \left( n \right)$ (costo della ricombinazione)
\end{enumerate}

\begin{itemize}
\item $a$ = numero di sottoproblemi che risulta dalla fase di divide
\item $\frac{n}{b}$ = dimensione di ciascun sottoproblema
\end{itemize}

Eseguire l'\textbf{\href{ftp://ftp.elet.polimi.it/users/Luca.Breveglieri/Laurea/Fondamenti di Informatica II/2000-2001/EqRicor.pdf}{unfolding}} significa sviluppare alcuni passi di un'equazione alle ricorrenze per trovare una forma analitica chiusa della ricorrenza stessa sotto forma di sommatoria. Serve per analizzare la complessità di una funzione ricorsiva.

\subsection{Ricerca binaria}
\paragraph{Equazione alle ricorrenze}
\[
\begin{cases} D \left( n \right) = \Theta \left( 1 \right) \\
C \left( n \right) = \Theta \left( 1 \right) \\
a = 1, \, b = 2 \end{cases} \rightarrow \begin{cases} T \left( n \right) = T \left( \frac{n}{2} \right) + 1 \cancel{+ 1} , \quad n > 1 \\
T \left( 1 \right) = 1 , \quad n = 1 \end{cases}
\]

\paragraph{Unfolding}
\begin{enumerate}
\item sviluppo: $T \left( n \right) = T \left( \frac{n}{2} \right) + 1 = T \left( \frac{n}{4} \right) + 1 + 1 = \cdots = T \left( \frac{n}{2^i} \right) + \sum 1 = \cdots$
\item condizione di terminazione: $T \left( \frac{n}{2^i} \right) = T \left( 1 \right) ; \; \frac{n}{2^i} = 1 ; \; i = \log_2{n}$\footnote{Per semplicità si suppone $n$ una potenza esatta di 2.}
\item sommatoria: $T \left( n \right) = \sum_{i = 0}^{\log_2{n}} 1 = 1 + \log_2{n}$
\item complessità asintotica: $T \left( n \right) = O \left( \log{n} \right)$ $\Rightarrow$ algoritmo logaritmico
\end{enumerate}

\subsection{Ricerca del massimo di un vettore}
\paragraph{Equazione alle ricorrenze}
\[
\begin{cases} D \left( n \right) = \Theta \left( 1 \right) \\
C \left( n \right) = \Theta \left( 1 \right) \\
a = 2, \, b = 2 \end{cases} \rightarrow \begin{cases} T \left( n \right) = 2 T \left( \frac{n}{2} \right) + 1 \cancel{+ 1} , \quad n > 1 \\
T \left( 1 \right) = 1 , \quad n = 1 \end{cases}
\]

\begin{itemize}
\item $a = 2$: il vettore viene suddiviso in due sottovettori
\item $b = 2$: ciascun sottovettore è ampio la metà del vettore di partenza
\end{itemize}

\paragraph{Unfolding}
\begin{itemize}
\item $T \left( n \right) = 1 + 2 + 4 + 8 T \left( \frac{n}{8} \right) = 1 + 2 + 4 + \cdots = \sum_{i = 0}^{\log_2{n}} 2^i$
\item progressione geometrica\footnote{Si veda la sezione \href{https://it.wikipedia.org/wiki/Progressione geometrica\#Serie geometrica}{Serie geometrica} nella voce Progressione geometrica su Wikipedia in italiano.} $\Rightarrow$ $T \left( n \right) = \frac{2^{\log_2{n} + 1} - 1}{2-1} = 2^{\log_2{n} + 1} - 1 = 2n - 1$
\item $T \left( n \right) = O \left( n \right)$ $\Rightarrow$ algoritmo lineare
\end{itemize}

\subsection{Moltiplicazione rapida di 2 interi}
\[
x \cdot y = x_s \cdot y_s \cdot {10}^n + x_s \cdot y_d \cdot {10}^{\frac{n}{2}} + x_d \cdot y_s \cdot {10}^{\frac{n}{2}} + x_d \cdot y_d
\]

\paragraph{Equazione alle ricorrenze}
\[
\begin{cases} D \left( n \right) = \Theta \left( 1 \right) \\
C \left( n \right) = \Theta \left( n \right) \\
a = 4, \, b = 2 \end{cases} \rightarrow \begin{cases} T \left( n \right) = 4 T \left( \frac{n}{2} \right) + n \cancel{+ 1} , \quad n > 1 \\
T \left( 1 \right) = 1 , \quad n = 1 \end{cases}
\]

\begin{itemize}
\item $C \left( n \right) = \Theta \left( n \right)$: per eseguire la somma binaria di due numeri di $n$ cifre occorre scandire ciascuna cifra
\item $a = 4$: i problemi ricorsivi sono le 4 operazioni di prodotto (le somme hanno complessità lineare ma non sono ricorsive)
\item $b = 2$: $x_s$, $x_d$, $y_s$ e $y_d$ sono ampi la metà dei vettori di partenza
\end{itemize}

\paragraph{Unfolding}
\begin{itemize}
\item $T \left( n \right) = n + 4 \cdot \frac{n}{2} + 4^2 \cdot \frac{n}{4} + 4^3 \cdot T \left( \frac{n}{8} \right) = n \left( 1 + 2 + 4 + \cdots \right) = n \sum_{i = 0}^{\log_2{n}} 2 ^ i$
\item progressione geometrica $\Rightarrow$ $T \left( n \right) = n \cdot \left( 2^{\log_2{n} + 1} - 1 \right) = 2 n^2 - n$
\item $T \left( n \right) = O \left( n^2 \right)$ $\Rightarrow$ algoritmo quadratico
\end{itemize}

\subsection{Moltiplicazione rapida di 2 interi ottimizzata}
\paragraph{Equazione alle ricorrenze}
\[
a=3 \rightarrow T\left( n \right) = 3 T \left( \frac{n}{2} \right) +n
\]

\paragraph{Unfolding}
\begin{itemize}
\item $T \left( n \right) = n+3 \cdot \frac{n}{2} + 3^2 \cdot \frac{n}{4} + 3^3 \cdot T \left( \frac{n}{8} \right) = n \left[ 1 + \frac{3}{2} + { \left( \frac{3}{2} \right) }^2 + \cdots \right] = n \sum_{i = 0}^{\log_2{n}} {\left( \frac{3}{2} \right)}^i$
\item progressione geometrica $\Rightarrow$ $T \left( n \right) = 2n \left[ {\left( \frac{3}{2} \right)}^{\log_2{n} + 1} - 1 \right] = 3 \cdot 3^{\log_2{n}} - 2n$
\item per la proprietà dei logaritmi: $a^{log_b{n}} =n^{log_b{a}} \rightarrow T \left( n \right) = 3 \cdot n^{log_2{3}} \cancel{-2n}$
\item $T \left( n \right) = O \left( n^{\log_2{3}} \right)$ $\Rightarrow$ più efficiente della moltiplicazione non ottimizzata ($\log_2{3} < 2$)
\end{itemize}

\subsection{Le Torri di Hanoi}
Ho $k = 3$ pioli verticali, con $n = 3$ dischi forati in ordine decrescente di diametro sul primo piolo. Si vuole portarli tutti e 3 sul terzo piolo. Condizioni per lo spostamento:
\begin{enumerate}
\item si può spostare un solo piolo per volta;
\item sopra ogni disco solo dischi più piccoli.
\end{enumerate}

Il problema si suddivide in 3 sottoproblemi:
\begin{enumerate}
\item problema da $n - 1$: 000 $\rightarrow$ 011 (2 deposito) $\Rightarrow$ si suddivide a sua volta in 3 sottoproblemi elementari
\item problema da 1: 011 $\rightarrow$ 211 $\Rightarrow$ sottoproblema elementare
\item problema da $n - 1$: 211 $\rightarrow$ 222 (0 deposito) $\Rightarrow$ si suddivide a sua volta in 3 sottoproblemi elementari
\end{enumerate}

\paragraph{Equazione alle ricorrenze} $T \left( n \right) = 2T \left( n - 1 \right) +1$
\begin{enumerate}
\item \ul{dividi:} $D \left( n \right) = \Theta \left( 1 \right)$ (considero $n- 1$ dischi)
\item \ul{risolvi:} $2 T \left( n - 1 \right)$ (ho 2 sottoproblemi ciascuno da $n- 1$)
\item \ul{impera:} $\Theta \left( 1 \right)$ (termino quando sposto un disco solo)
\item \ul{combina:} $C \left( n \right) = \Theta \left( 1 \right)$ (nessuna combinazione)
\end{enumerate}

\paragraph{Unfolding}
\begin{itemize}
\item $T \left( n \right) = 1+2+4+8T \left( n-3 \right) = 2^0 + 2 ^1 + 2^2 + \cdots$
\item $n - i = 1; \; i = n - 1 \rightarrow T \left( n \right) = \sum_{i = 0}^{n - 1} 2^i$
\item progressione geometrica $\Rightarrow$ $T \left( n \right) = 2^n \cancel{-1}$
\item $T\left( n \right) = O \left( 2^n \right)$ $\Rightarrow$ algoritmo esponenziale (anche se decidibile)
\end{itemize}

\subsection{Il righello}
Disegnare le tacche di un righello in maniera ricorsiva, di differenti altezze, fino a quando si arriva all'altezza 0. Si disegna ricorsivamente a metà del sottointervallo la tacca, quindi si considerano i due sottointervalli sinistro e destro e si disegna la tacca di una unità di altezza inferiore.

\paragraph{Backtrack} Si esplora una scelta per volta, e se la scelta non va bene si ritorna indietro. (vd. filo di Arianna) Si termina quando tutte le scelte sono esaurite. Tramite il backtrack si può esplorare tutto lo spazio delle soluzioni.

\section{Gli algoritmi ricorsivi di ordinamento}
\subsection{Merge sort (o ordinamento per fusione)}
\subsubsection{Passi}
\begin{enumerate}
\item \ul{divide:} si suddivide il vettore in due sottovettori
\item \ul{ricorsione:}
\begin{itemize}
\item si applica il merge sort sul sottovettore sinistro
\item si applica il merge sort sul sottovettore destro
\item condizione di terminazione: il sottovettore ha 1 cella
\end{itemize}
\item \ul{combina:} si uniscono tutti i sottovettori ordinati, scandendo in parallelo i due sottovettori da sinistra verso destra e confrontando di volta in volta i valori scegliendo quello minore:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\linewidth]{pic/06/Ricombinazione_merge_sort.png}
\end{figure}
\end{enumerate}

\subsubsection{Analisi di complessità}
Ragionamento intuitivo: ho $\log_2{n}$ livelli di ricorsione e devo fare $\frac{n}{2} + \frac{n}{2} = n$ unioni $\Rightarrow$ $n \log_2{n}$ operazioni totali:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\linewidth]{pic/06/Merge_sort_intuitivo.png}
\end{figure}

\paragraph{Equazione alle ricorrenze}
\[
\begin{cases} T \left( n \right) = 2 T \left( \frac{n}{2} \right) + n, \quad n > 1 \\
T \left( 1 \right) = 1 , \quad n = 1 \end{cases}
\]

\begin{enumerate}
\item \ul{dividi:} $D \left( n \right) = \Theta \left( 1 \right)$ (calcola la metà di un vettore)
\item \ul{risolvi:} $2T \left( \frac{n}{2} \right)$ (risolve 2 sottoproblemi di dimensione $\frac{n}{2}$ ciascuno)
\item \ul{terminazione:} $\Theta \left( 1 \right)$ (semplice test)
\item \ul{combina:} $C \left( n \right) = \Theta \left( n \right)$
\end{enumerate}

\paragraph{Unfolding}
\[
T \left( n \right) = n + 2 \cdot \frac{n}{2} + 2^2 \cdot \frac{n}{4} + 2^3 T \left( \frac{n}{8} \right) = \sum_{i = 0}^{\log_2{n}} n = n \sum_{i = 0}^{log_2{n}} 1 = n \left( 1 + \log_2{n} \right) = n \log_2{n} \cancel{+ n}
\]

$T \left( n \right) = O \left( n \log{n} \right)$ $\Rightarrow$ algoritmo linearitmico (ottimo)

\subsection{Quicksort}
\subsubsection{Passi}
È quadratico ($O \left( n^2 \right)$) $\Rightarrow$ secondo la teoria degli algoritmi non dovrebbe essere ottimo, però: sperimentalmente si dimostra che il caso peggiore ricorre molto raramente, a differenza del caso medio.

\paragraph{Partizione} La divisione non avviene a metà, ma secondo una certa proprietà: sceglie arbitrariamente un elemento separatore (= \textbf{pivot}), quindi separa gli altri valori in sottovettore sinistro e destro a seconda se sono minori o maggiori del pivot.

Individua (tramite un ciclo ascendente su $i$ e un ciclo discendente su $j$) una coppia di elementi di indici $i$ e $j$ che siano entrambi fuori posto (ovvero l'$i$-esimo è maggiore del pivot, e il $j$-esimo è minore del pivot), quindi li scambia.

Condizione di terminazione: \texttt{i == j} (si ferma quando tutte le coppie sono già a posto).

Costo della partizione: $T \left( n \right) = \Theta \left( n \right)$ (cioè la scansione di tutti gli $n$ elementi).

Al termine della ricorsione è inutile una fase di ricombinazione, perché i dati si trovano già nella posizione corretta.

\subsubsection{Analisi di complessità}
Il quicksort non segue la formula generale del divide et impera.

\paragraph{Caso peggiore}
Il vettore si trova in ordine inverso $\Rightarrow$ la partizione genera un sottovettore da $n- 1$ elementi e l'altro da 1 elemento.

\subparagraph{Equazione alle ricorrenze}
\[
T \left( n \right) = T \left( 1 \right) +T \left( n-1 \right) +n+1= T \left( n-1 \right) +n \cancel{+2}
\]

\begin{itemize}
\item $T \left( 1 \right)$ = costo per il sottovettore da 1 elemento
\item $T \left( n - 1 \right)$ = costo per il sottovettore da $n- 1$ elementi
\item $n$ = costo della partizione
\item $1$ = costo unitario della risoluzione (riguarda gli elementi al termine della ricorsione)
\end{itemize}

\subparagraph{Unfolding}
\[
T \left( n \right) = n+T \left( n -1 \right) = n+ \left( n-1+T \left( n-2 \right) \right) = \cdots = \frac{n}{2} \left( n+1 \right)
\]

\paragraph{Caso migliore}
Ogni sottovettore è esattamente la metà $\Rightarrow$ ricorda il merge sort (in questo caso è linearitmico).

\paragraph{Caso medio}
Partizione fortemente sbilanciata, senza arrivare al caso peggiore $\Rightarrow$ il quicksort è linearitmico.

È meglio cercare un pivot in modo da dividere sempre in maniera conveniente il vettore $\Rightarrow$ la complessità non cambia (neanche prendendo un pivot che separa sempre il vettore a metà), ma si possono ridurre le costanti.