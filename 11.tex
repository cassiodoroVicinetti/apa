\chapter{Le tabelle di hash}
La \textbf{tabella di hash} è un tipo di dato astratto che utilizza uno spazio di memoria congruente con il numero di dati, in cui si garantiscono operazioni prossime al costo unitario, ovvero con un tempo medio di accesso in ricerche/cancellazioni/inserzioni di tipo unitario.

\begin{itemize}
\item \ul{tabelle ad accesso diretto:} la chiave è l'indice del vettore $\Rightarrow$ complessità lineare, ma: non sono utilizzabili quando il numero delle chiavi è troppo grande e non si può allocare in memoria;
\item \ul{strutture ad albero:} complessità logaritmica, ma si spera che l'albero non degeneri;
\item \ul{tabelle di hash:} c'è una funzione intermedia detta \textbf{funzione di hash} che manipola la chiave e la trasforma in un indice.
\end{itemize}

Il numero $\left| K \right|$ delle chiavi contenute in una tabella di hash di dimensione $M$ è molto minore della cardinalità dell'universo delle chiavi: $\left| K \right| \ll \left| U \right|$.

Se ogni chiave nell'universo ha un indirizzo compreso tra $0$ e $m - 1$, la funzione di hash $h: U \longrightarrow \left \{ 0, \cdots ,m-1 \right \}$ associa alla chiave $k$ l'indice $h \left( k \right)$ della tabella $T$.

La tabella di hash deve garantire una buona distribuzione: se le chiavi sono equiprobabili allora i valori $h \left( k \right)$ devono essere equiprobabili, cioè la distribuzione deve essere \textbf{semplice uniforme} e non si deve polarizzare su determinate chiavi.

\section{Tabelle di hash per valori numerici}
\subsection[Metodo moltiplicativo (chiavi \textit{k} in virgola mobile)]{Metodo moltiplicativo \small{(chiavi $k$ in virgola mobile)}}
Per una chiave $k$ compresa nell'intervallo $\left[ s , t \right]$:
\[
h \left( k \right) = \sup{\left( M \frac{k - s}{t-s} \right)}
\]

\begin{enumerate}
\item normalizzazione: si divide $k-s$ (offset tra la chiave $k$ e l'estremo inferiore dell'intervallo) e $t-s$ (ampiezza dell'intervallo) $\Rightarrow$ si ottiene un valore compreso tra 0 e 1;
\item si moltiplica per il numero $M$ per trovare un valore compreso nell'intervallo tra 0 e $m-1$;
\item si prende l'intero superiore, poiché $h \left( k \right)$ è un numero intero.
\end{enumerate}

\subsection[Metodo modulare (chiavi \textit{k} intere)]{Metodo modulare \small{(chiavi $k$ intere)}}
Si applica la funzione \texttt{mod()} a una chiave \texttt{k} intera:
\[
h \left( k \right) = k \, \text{mod} \, M
\]

Per limitare le collisioni, si prende per $M$ un numero primo. Esempi antitetici di tabelle di hash sono la funzione $\text{mod} \, 2^n$: concentra tutti i numeri in valori 0 e 1 $\Rightarrow$ tantissime collisioni perché si limita ai primi n bit più significativi.

\subsection[Metodo moltiplicativo-modulare (chiavi \textit{k} intere)]{Metodo moltiplicativo-modulare \small{(chiavi $k$ intere)}}
Data una costante $A$ (per esempio: $A= \tfrac{\sqrt{5} - 1}{2}$):
\[
h \left( k \right) = \inf {\left( k \cdot A \right)} \, \text{mod} \, M
\]

\section{Tabelle di hash per stringhe}
\subsection{Metodo modulare per stringhe corte}
Pensando ogni carattere come numero, la stringa è la rappresentazione compatta di un polinomio. Valutando il polinomio in un punto (di solito $\text{card} \left( U \right)$), si ottiene il valore numerico intero $k$ $\Rightarrow$ si applica il metodo modulare per chiavi intere.

Le operazioni per valutare il polinomio non sono però efficienti $\Rightarrow$ questo metodo si può usare per stringhe corte.

\subsection{Metodo modulare per stringhe lunghe}
La valutazione del polinomio è effettuata tramite il \textbf{metodo di Horner}, che considera ricorsivamente polinomi di grado 1:
\[
p_n \left( x \right) = \left \{ \left[ \left( a_n x+a_{n-1} \right) x+ a_{n-2} \right] x + \cdots + a_1 \right \} x + a_0
\]

La base $x$ del polinomio può essere un numero primo o un numero pseudocasuale \texttt{a = (a * b) mod (M - 1)} (\textbf{hash universale}). Per calcolare la chiave $k$, per ogni polinomio di primo grado valutato si deve fare a ogni passo la funzione \texttt{mod M}.

\section{Gestione delle collisioni}
È inevitabile il fenomeno della \textbf{collisione}, ovvero quando chiavi diverse corrispondono allo stesso valore di indice; si può ridurre con buone funzioni di hash.

Si ha una collisione se:
\[
k_i \neq k_j \longrightarrow h \left( k_i \right) = h \left( k_j \right)
\]

\subsection{Linear chaining}
Ogni elemento della tabella contiene un puntatore alla testa di una lista concatenata che contiene tutte le chiavi collidenti.

\paragraph{Operazioni sulla lista}
\begin{itemize}
\item \ul{inserzione:} non si hanno esigenze di ordinamento $\Rightarrow$ l'inserzione meno costosa è quella in testa: $T \left( n \right) = O \left( 1 \right)$;
\item \ul{ricerca:} se $N = \left| K \right|$ = numero delle chiavi, $M$ = dimensione della tabella di hash:
\begin{itemize}
\item caso peggiore: tutte le chiavi si concentrano in una sola lista $\Rightarrow$ operazione lineare nella dimensione della lista, ma: caso poco frequente: $\Theta \left( N \right)$;
\item caso medio: le chiavi sono tutte uniformemente distribuite $\Rightarrow$ la dimensione media delle liste è uguale al fattore di carico $\alpha = \tfrac{N}{M}$: $T \left( n \right) = O \left( 1+ \alpha \right)$ (un buon valore di $\alpha$ è 0,1);
\end{itemize}
\item cancellazione:
\begin{itemize}
\item se la lista è doppio-linkata: $T \left( n \right) = O \left( 1 \right)$;
\item altrimenti, il costo è legato alla ricerca dell'elemento.
\end{itemize}
\end{itemize}

\subsection{Open addressing}
Ogni cella può contenere al massimo un solo elemento ($\alpha \leq 1$), e in caso di collisione il valore da inserire va inserito in un'altra cella, verificando che sia vuota (\textbf{probing} = ricerca di una cella vuota). Le $M-1$ celle rimanenti vengono sondate nell'ordine di una delle $\left( M - 1 \right) !$ permutazioni stabilita dalla funzione di hash, in funzione anche del tentativo $t$: $h \left( k,t \right) : U \times \left \{ 0, \cdots , M -1 \right \} \longrightarrow \left \{ 0, \cdots ,M-1 \right \}$.

\subsubsection{Linear probing}
\begin{itemize}
\item \ul{inserzione:} si sonda la cella di indice successivo e si inserisce nella prima casella vuota;
\item \ul{ricerca:} si passa di volta in volta alla cella di indice successivo, ricordando che al termine della tabella si deve passare alla prima cella (\texttt{mod M} a ogni incremento). È una gestione implicita delle liste all'interno di un vettore senza ricorrere al concetto di puntatore;
\item \ul{cancellazione:} bisogna distinguere le celle vuote dalle celle svuotate dalla cancellazione, perché altrimenti la ricerca successiva si fermerebbe alla cella svuotata.
\end{itemize}

Sperimentalmente, tentativi in media di probing per la ricerca:
\begin{itemize}
\item search miss: $\frac{1}{2} \left( 1 + \frac{1}{1 - \alpha} \right)$
\item search hit: $\frac{1}{2} \left( 1+ \frac{1}{{\left( 1- \alpha \right)}^2} \right)$
\end{itemize}

Se $\alpha \sim 0^+$ $\Rightarrow$ search miss $\approx 1$ $\Rightarrow$ efficiente.

\subsubsection{Double hashing}
L'indice della cella successiva in caso di probing con insuccesso viene calcolato da un'altra funzione di hash $h_2$.

Sperimentalmente, tentativi in media di probing per la ricerca:
\begin{itemize}
\item search miss: $\frac{1}{1- \alpha}$
\item search hit: $\frac{1}{\alpha} \log{\frac{1}{1 - \alpha}}$
\end{itemize}